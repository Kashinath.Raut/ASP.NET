--Sample Table
CREATE TABLE Users
( 
	User_Id INT PRIMARY KEY NOT NULL,
  	Name VARCHAR(50) NOT NULL,
      flag BIT DEFAULT 'true'
)
	


--Procedure to delete User--

CREATE PROCEDURE DeleteUser
--parameter: id of User to delete
@Id  int


AS
  BEGIN

      -- SET NOCOUNT ON to prevent extra messages Showing Number of affected rows
      SET NOCOUNT ON;

      UPDATE Users 
      SET flag='false'
      WHERE User_Id=@Id;

  END



--Procedure call
--DeleteUser 1        --set flag='false' of user having User_Id=@Id


