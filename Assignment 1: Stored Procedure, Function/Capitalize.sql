CREATE FUNCTION Capitalize
(
	@Name VARCHAR(50)	 --string input
)
RETURNS VARCHAR(50)      --returns Capitalized string


AS
 BEGIN

	DECLARE @CapitalizedName VARCHAR(50),@index INT	
	SET @CapitalizedName = ' '             
	SET @index = 2                     

	SET @CapitalizedName = UPPER(LEFT(@Name,1))	  --make first letter capital

	WHILE(@index <= LEN(@Name))		 --traverse through the string searching for space
		BEGIN
			IF SUBSTRING(@Name,@index-1,1) = ' '   --check if previous character is space ' '  
				BEGIN
					SET @CapitalizedName = @CapitalizedName + UPPER(SUBSTRING(@Name,@index,1)) 
					    --if previous character is space then capitalize the next character and concatenate to previous substring
				END
			ELSE
				BEGIN
					SET @CapitalizedName = @CapitalizedName + LOWER(SUBSTRING(@Name,@index,1))	
					    --Lowercase All other characters and concatenate
				END
			SET @index = @index + 1
		END

	RETURN @CapitalizedName		--Resulted(Capitalized) Name

 END
 
 
--Function Call
--SELECT * FROM Capitalize('john carter')

--OUTPUT--
--John Carter



