--Sample Table
CREATE TABLE Users
( 
	User_Id INT PRIMARY KEY NOT NULL,
  	Name VARCHAR(50) NOT NULL
)
	


--Procedure to Insert/Update the User--

CREATE PROCEDURE InsertUpdate 
--Parameters for Procedure
--Here we are getting User_Id and Name As a Input and update/insert record based on User_id
@Id  INT,
@Name VARCHAR(50)	


AS
 BEGIN

	  -- SET NOCOUNT ON to prevent extra messages Showing Number of affected rows
      SET NOCOUNT ON;

      IF EXISTS (SELECT 1 FROM  Users  WHERE  User_Id = @Id)				--check if User with Id already exist
        	UPDATE Users														--UPDATION
        	SET Name = @Name WHERE User_Id=@Id

      ELSE
        	INSERT INTO Users([User_Id],  [Name]) VALUES(@Id, @Name)			--INSERTION

 END



--Procedure Call

--InsertUpdate 1,'john carter'	    --insert new record
--InsertUpdate 2,'garry curston'	--insert new record
--InsertUpadte 1,'steve smith'  	--update record having User_Id=1



